FROM debian:buster-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      pandoc pandoc-citeproc python3-pip python3-setuptools \
      make texlive texlive-latex-extra texlive-fonts-extra lmodern && \
    apt-get clean && \
    pip3 install pantable
