---
title: Application of modern techniques to real-time and robust system design
author:
  - "Author: Peter Majchrak"
  - "Supervisor: Erik Jacobsen"
date: 2019-06-07
lang: en
rights: © 2019 Peter Majchrak, CC BY-SA 4.0
fontfamily: dejavu
bibliography: bibliography.bib
csl: ieee-with-url.csl
link-citations: true
toc: true
numbersections: true
papersize: A4
---

\pagebreak

# Glossary

```table
---
caption: Glossary
alignment: RL
table-width: 1
markdown: True
width: [0.2, 0.8]
---
Term, Definition
AFL, American Fuzzy Lop (fuzzer; not the rabbit)
ALSA, Advanced Linux Sound Architecture
COBS, Consistent Overhead Byte Stuffing
CRC, Cyclic Redundancy Check
EASA, European Union Aviation Safety Agency
FAA, Federal Aviation Administration
FFT, Fast Fourier Transform
JSON, JavaScript Object Notation
JVM, Java Virtual Machine
OS, Operating System
PA, PulseAudio
RTOS, Real Time Operating System
SDL, Simple DirectMedia Layer
SFML, Simple and Fast Multimedia Library
```

\pagebreak

# Introduction & Project Description

In this bachelor project I want to apply the skills taught as part of the
subjects like system integration and testing, to build a semi-complex soft
real-time and robust system. More specifically, I will be building a program for
running real-time visualizations, based primarily on audio data, and to
accomplish this I will integrate components such as OS audio subsystem, audio
processing, scripting engines and various output devices all while trying to
keep minimum latency.

During the course of this project I also want to explore what are the common
problems that projects like these face, from the software development
perspective and how can we address them.

## Project's Purpose

Some time ago, I reverse engineered an LED clock I bought as a kit from eBay:

![LED Clock](./assets/led_clock.jpg)

I figured out the chip used and the display wiring. After that I quickly wrote
some new firmware for it to display whatever I want. But every time I wanted
something new to be displayed I had to reprogram the chip inside, which was
annoying. So I thought to myself, I can send it image data over serial, and
that's basically how this project originated. I wanted it to react to music,
display a spectrogram and run some nice audio visualizations on it.

At first, this was supposed to be just another hobby project of mine, but due to
some unexpected consequences at my work, I was not able to do my bachelor
project at the same company I was doing my internship in. This led me to doing
this as my bachelor project.

Of course, this project will focus purely on the software side of things.
Originally I was thinking of just hacking something up quickly to to what I want
it to do. But now that I have more time dedicated to this, let's make it a more
general solution and at the same time try to explore how we can use various
skills from the academy at developing such a real-time system.

## Expected Result

The primary objective is to have a program that can get audio data from the OS,
do a little bit of analysis, pass the data through a user supplied script and
display the result. In more detail:

- soft real-time audio analysis and visualization
- scriptable step in the visualization pipeline
- multiple inputs (PulseAudio, ALSA, SDL, SFML, etc.)
- multiple outputs (various kinds of displays, emulated hardware, etc.)
- environment for easy development of visualizations in real-time

Secondary goals are gaining insight into the development of real-time systems
and how they differ from regular software. I also want to see how to transfer
the skills from various courses of the bachelor program in software development
to the real-world where we might no longer have the niceties of the JVM. More
specifically, testing, system integration and large systems development.

# Real-time systems

Real-time systems are computer systems that are guaranteed to respond to an
event withing a certain timeframe. These kinds of systems are often needed for
safety-critical applications where a delayed response can cause harm or system
failure. There are several classifications of real-time systems, one of them
being the criticality classification, describing how important it is to deliver
a response in a given timeframe.

 - hard real-time: missing the deadline means a complete system failure
 - firm real-time: response after the deadline has no value, the system can still continue operation
 - soft real-time: value of the response decreases after the deadline, missed deadlines degrade service quality

Timeframes of such systems depend on their specific use case, often ranging from
nanoseconds to milliseconds or even seconds. Real-time systems are often used in
military and other mission-critical applications like medical devices and
implants, spacecraft, vehicle or airplane autopilot and various other kinds of
control loops, all of which operate in slightly different timescales and have
different real-time requirements.

Every industry with the need for real-time systems will have regulations and
guidelines that need to be met in order to obtain the necessary permissions to
sell or use the system in a production environment. For example in the aerospace
industry, systems will often have to follow the guidelines of DO-178C, Software
Considerations in Airborne Systems and Equipment Certification and its companion
documents to get approved by FAA or EASA.

# Robust systems

Robust systems are systems that can handle runtime errors gracefully and
recover. This can include various error states and conditions caused by
underlying hardware or firmware but also erroneous input from users or other
actors. Robustness is often a hard requirement for safety-critical applications
in the industrial, commercial and medical sectors. Systems that are not robust
can often be exploited in ways ranging from denial of service to arbitrary code
execution.

There are multiple techniques and strategies to develop these systems. One of
which is fuzzing, which has seen a large adoption in big projects like Chromium
and ffmpeg because of its track record of finding vulnerabilities. We will talk
more about fuzzing in a later chapter.

Other option is to inject faults into the system. This can be done either at
runtime or compile time. Compile time fault injection systems usually instrument
the code with extra statements that modify variable values or memory content.

Runtime fault injection can be done in various ways but most commonly its done
by intercepting system calls. It is a fairly simple method because it does not
require recompiling the executable from source.

In the circle of esoteric programming languages, there exists a category of
non-deterministic or probabilistic languages that do something very similar.
Languages like Java2K [@gerson_kurz_java2k_2019] and Ambi-F that belong to this
category, only have a certain probability of executing your code correctly.
Apart from the original joke purpose of these languages, I think they illustrate
a good point about systems and their reliability. Moreover, designing more and
more complex distributed systems that scale, requires a fault-tolerant
architecture that no longer assumes 100% reliable hardware [@ackley2013cacm]
[@ackley2011hotos]. This form of runtime fault injection is a good way to test
robustness of our algorithms.

In this project, besides the more well known testing techniques, I want to try
out fuzzing and leave the runtime fault injection strategies mostly in the
theoretical domain for now, even though they are good candidates for testing
robustness of systems.

\pagebreak

# System requirements

Before we can start designing something, we need to know what we want to build
and what requirements it needs to satisfy. In many cases, this is in a form of a
prioritized list of requirements, supplied by the client. There are multiple
ways to prioritize it, one is to just use regular numbers, but the MoSCoW method
is also frequently used.

In our case the list of requirements would look something like this:

 - M: visualize audio in real-time
 - M: run on linux and macOS
 - S: easily scriptable
 - S: intuitive user interface
 - S: auto-detect changes in the user's script
 - C: have multiple input/output types
 - C: change input/output at runtime
 - W: have a GUI
 - W: have an extensive library of processing routines

## Real-time system requirements

For real-time systems we often need additional requirements so that the
product's behavior can be used in real-time or other safety critical
applications. The main characteristic of a real-time system is its guaranteed
response to a event within a certain timeframe whenever possible. Timeframes can
very from nanoseconds to milliseconds or even a few seconds based on the
specific use case.

For our example system this could be:

  - 44.1 kHz audio sampling frequency
  - 1024 sample wide buffers
  - 43 Hz buffer fill rate => 23.5 ms to fill up sample buffer
  - 10 ms for signal preprocessing
  - 10 ms for visualization script

To keep up with buffer delivery, each processing stage will need to be able to
process a buffer in under 23.5 ms, otherwise the unprocessed buffers will start
piling up. The processing stages can be pipelined as shown in figure
\ref{timing_diagram}. This allows us to use more total processing time per
buffer at the cost of delay. Ideally the delay would not exceed 3 frames.

![Approximate timing diagram for our example audio analysis
system\label{timing_diagram}](./assets/timing.png)

Because our example application is running on an operating system there can be
additional scheduling uncertainties. To address this issues we can use a
real-time operating system. In our case, the easiest solution is to use the
real-time version of the Linux kernel. For more critical systems it would be
important to use RTOS during operation to minimize the scheduling latencies,
however for our audio visualization system it is not nearly as important. In our
case, nothing bad happens if we drop couple animation frames.

## Robust system requirements

Our system is going to handle the following:

  - audio samples in F32 format
  - preprocessing the audio data (FFT, power envelope, etc.)
  - script code and execution (python, js, etc.)
  - results of running the script
  - encoding and decoding the data for the output device

For each of these, we should handle all possible errors that can occur. How to
handle an error can differ based on the severity of the error, for example if we
receive a NaN value in our audio we can just replace it by the average of its
two neighbors and continue without ever having to alert the user to the problem.
On the other hand, issues like syntax errors in the user supplied script should
be shown to the user and not continue operation until the user fixes the script.

The output device codec needs special attention because protocol codecs are a
very frequent source of errors. Programs written in C for example, often suffer
from issues like null pointer dereference, segmentation faults because of
invalid pointer arithmetic or use after free, etc.
[@mateusz_jurczyk_ffmpeg_2014]. In this case it would be nice to have an
extensive test suite along with some fuzz testing.

\pagebreak

# Choosing a development methodology

Factors governing the choice of a development methodology used in
safety-critical, real-time and robust systems can be quite different from
conventional software development. This is due to the specific requirements that
a lot of these systems have. Usually, these small embedded systems do not or
cannot have software or firmware upgrade functionality. There can be many
reasons why this is the case, from security and safety of users to cost saving
measures. For a lot of these projects it is important to get everything right on
the first try, and we as software developers know that that is hard to achieve.

Systems that are one-time programmable or their firmware cannot be upgraded in
the field are potentially suitable for a sequential development models like
waterfall or the V model. People might even think that because there is only one
time we deliver a product, we should use one of these sequential models. This
unnecessary limitation means, we might miss out on some of the advantages of
other options like agile strategies, that can also be adapted for these types of
projects.

## Agile methodologies

In the recent years, we have seen a big increase in use of agile in the
workplace. Even federal IT projects saw a huge increase in use of agile
methodologies, from only 10% adoption in 2011 to 80% in 2017
[@viechnicki_agile_2017].

I think the iterative nature of agile gives it a lot of advantages over
traditional approaches. Getting feedback and being able to make and fix mistakes
is critical in my opinion. Bugs and wrong design decisions will always happen,
so it is important to have the opportunity to fix them.

Even if our final device might be one time programmable and will have no
possibility of firmware upgrades, we should still iterate on the design and
software until we get it right (enough). Having a development device where we
can find bugs and run tests is critical, because sometimes emulation is not
perfect and can produce different results than the real hardware. These
iterations should happen both in the small cycles of development iterations but
also the bigger, overarching design and architecture level iteration. That's why
there are often multiple similar versions of the product on the market, fixing
bugs, adding features and tweaking the design are very probable causes.

Without going too much in depth, I think agile strategies are the next
evolutionary step in software development, where a more organic structure can
yield better efficiency over the traditional dictatorial models.

\pagebreak

# Implementation

One of the key features of a project like this is the importance of system
integration. We have all these subsystems that we need to integrate in order to
get the desired result. In our case that would mean integrating the audio
subsystem of the operating system or a library, audio processing, scripting
engines and finally our output devices. I used various methods from the System
integration course of PBA to achieve this. Most importantly, I used asynchronous
communication via message passing and queues to allow these subsystems to
communicate. Among other things, this is why I used the Rust programming
language for implementing this system, as its standard library includes things
like in-memory queues and asynchronous message channels.

![Illustration of pipeline stages](./assets/pipeline.png){width=45%}

Different libraries and APIs have certain expectations about their usage, but
sometimes you just cant integrate with them directly. There is a lot of reasons
why this integration might get hindered. Common cause of issues is the impedance
mismatch of various APIs due to differences in schemas, interchange formats and
so on. Sometimes you might have an API that is poll based, meaning that you
constantly have to ask the API for a current value, but you want to integrate it
into your push or event based application. This means, you need a system that
will do the polling and send out a message only when the value changes.

This was the case for PulseAudio, audio subsystem used in practically all
unix-like OSes. The standard PA API is blocking, meaning that if you request to
read some audio data, your thread will be blocked until the data you requested
is received. However, I want to do other stuff while I'm reading the audio data.
This means I need to run this audio reading in its own thread and send the
resulting audio buffers to a message queue.

In the next step, another thread will pick up the buffer, do what it needs to do
with it and pass it along to another queue, until we finally reach the last
stage and send the data to a device. This way each processing stage is logically
acting as its own system that takes messages in, does some work and sends some
messages out. This is basically the actor model in practice. Good thing about
the actor model is that it scales nicely. It doesn't matter if the messages are
being passed along threads on the same machine or on machines spanning hundreds
of miles connected using the internet. As an aside, I would even say that
microservice designs are a form of actor model in certain cases. But of course,
for a real-time application we don't want to introduce additional network delays
so we will stick to the single machine for now.

In figure \ref{timing_diagram} you can see how this message passing works. Each
processing stage is in its own thread and when a new message arrives, it
performs the work needed and sends the output in a message to the next stage.
These messages are queued up in memory until the next stage can pick them up.
For a real time system, however, we don't want our queues to grow big, so our
processing stages will need to be able to process data at a certain rate as to
not stall the entire data pipeline. More precisely speaking, since our audio
buffers are coming in at a known rate, namely 43 Hz, we know that each stage has
to finish in a certain amount of time, that is 23.5 ms on average.

## Communication with hardware

If I wanted a robust system, I needed to implement a data transfer layer that
could detect and possibly even correct errors. This data transport will be used
to transfer data to the device using a serial link. There are several industry
standard ways to do this. One way it to use a checksum that can only detect that
an error occurred but can't fix it. Another way it to use stuff like Hamming
codes that can also perform forward error correction. For this application, I
decided to go with CRC checksums due to their relative simplicity. In this
transport layer I also used COBS for bundling the data into distinguishable
frames.

Decision to include a CRC was just a precautionary measure, because I was not
expecting to get transmission errors on the serial line. In the firmware I
actually don't even check the CRC validity due to limited time constraints.
Later, if I get the time to implement the CRC check in the firmware, I should
discard the frames that have errors but I think it might be better to display a
frame that's 99% correct than to discard a frame, thus behaving like we ignore
the checksum anyway. As I said, it is not necessary to have CRC in our
particular case where data quality is not of high concern.

The device has a 24x8 LED grid which can be easily represented by a 24 byte
pixel bitmap and that's the payload we will be sending.

![Output packet structure](./assets/packet_structure.png){width=50%}

## Architecture, Traits & Plugins

I approached the system with modularity in mind. Rust has an advanced type
system that allows us to use traits and associated types, which I used
extensively to model the different concepts present in the problem domain.

For input plugins I defined my own trait that has a method that returns a vector
of float audio samples. After that, you can implement this trait for your
specific plugin type.

```rust
trait Input {
    type Error;
    fn read(&mut self, len: usize) -> Result<Vec<f32>, Self::Error>;
}
```

Preprocessing and scripting stages have only one concrete implementation and
that is why I decided to not modularize them for now due to time constraints,
but I would recommend doing so in the future.

I designed the output stage such that it takes any Writable object and an
encoder. With this design we support wide range of devices and output protocols.
The encoder trait looks like this.

```rust
trait Encoder {
    type Payload;
    type Error;

    fn encode(p: &Self::Payload) -> Result<Vec<u8>, Self::Error>;
}
```

It takes any `Payload` type that comes from the script and encodes it into a
byte sequence that will get sent to the Writable output device.

We can then implement the encoder for our clock protocol where we specialize the
associated types `Payload` and `Error` (error handling is not ideal in this
case).

```rust
struct Clock;

impl Encoder for Clock {
    type Payload = [u8; 24];
    type Error = ();

    fn encode(p: &[u8; 24]) -> Result<Vec<u8>, ()> {
        let crc = { ... };

        let mut res = p.to_vec();
        res.extend_from_slice(&crc);

        let mut res = cobs::encode_vec(&res);
        res.push(0);

        Ok(res)
    }
}
```

This way we can, have a modular architecture where we can swap components in and
out to create a custom animation data flow.

![Modular architecture diagram](./assets/modular_architecture.png){width=80%}

This way each animation can choose what input plugin to use, what preprocessing
stage it needs, scripting engine could be chosen dynamically based on the file
extension of the supplied animation script, and finally the script would specify
which output device and protocol to use.

For the scripting stage I have integrated only with the Python interpreter so
far. Below, you can see an example of a visualization script written in Python.

```python
frame = 0

def visualize(input):
  global frame
  frame = frame + 1
  return [(frame + x) & 0xff for x in range(0, 24)]
```

The user script defines a function called `visualize` that gets called for each
frame, being passed the output of the preprocessing stage. Whatever this
function returns will get passed into the subsequent output stages. In this
example it would produce a moving binary counter pattern because each bit in the
result represents a pixel.

## CRC Code & Test Case generation

So I needed to choose one of many possible CRC algorithms for this task, but
existing CRC libraries for Rust offered only a limited set of them. And I don't
like being limited. Therefore, I created my own library
[@peter_majchrak_crcmod-rs_2019] with lots more algorithms to choose from. This
subproject required a degree of code generation to create an efficient CRC
library.

I was not able to find a complete list of all CRC algorithms currently in use by
the industry. However, I found a website listing a lot of popular CRCs and their
parameters [@gregcookCatalogueParametrisedCRC2019]. Unfortunately, this
information was not in a computer readable form so I wrote a script that parsed
this information out and created a JSON version of the data. This could then be
used for subsequent code generation steps. In figure \ref{reveng}, you can see
the CRC model data on the website before parsing and here you can see what data
looks like after parsing:

```json
{
  "width": 32,                "value_type": "u32",
  "poly": "0x04c11db7",       "init": "0xffffffff",
  "refin": "false",           "refout": "false",
  "xorout": "0xffffffff",     "check": "0xfc891918",
  "residue": "0xc704dd7b",    "codewords": ["redacted"],
  "name": "CRC-32/BZIP2",     "name_camel": "Crc32Bzip2",
  "name_lower": "crc32bzip2", "table": "redacted"
}
```

![Website showing CRC model parameters
(cropped).\label{reveng}](./assets/reveng.png)

Extracting the data was done by using a regular expression. Additionally I also
needed to extract codewords for generating test cases and that turned out to be
a bit trickier. I had to use XPath to find the correct HTML elements with
codewords and extract their text content. I also enriched the data with value
type used, lowercased and camelcased versions on the name and a CRC lookup
table.

Rust supports various kinds of code generation out of the box, which was very
helpful. I created a template that would be filled with data from the JSON
database during the build process.

```rust
use crate::*;

{% for m in models %}
  pub struct {{ m.name_camel }};

  impl Model<{{ m.value_type }}> for {{ m.name_camel }} {
      const POLY: {{ m.value_type }} = {{ m.poly }};
      const INIT: {{ m.value_type }} = {{ m.init }};
      const REFIN: bool = {{ m.refin }};
      const REFOUT: bool = {{ m.refout }};
      const XOROUT: {{ m.value_type }} = {{ m.xorout }};
      const CHECK: {{ m.value_type }} = {{ m.check }};
      const RESIDUE: {{ m.value_type }} = {{ m.residue }};
  }
{% endfor %}
```

After rendering the template, we will get the following Rust code. So each CRC
model becomes a type in Rust.

```rust
  pub struct Crc32Bzip2;

  impl Model<u32> for Crc32Bzip2 {
      const POLY: u32 = 0x04c11db7;
      const INIT: u32 = 0xffffffff;
      const REFIN: bool = false;
      const REFOUT: bool = false;
      const XOROUT: u32 = 0xffffffff;
      const CHECK: u32 = 0xfc891918;
      const RESIDUE: u32 = 0xc704dd7b;
  }
```

After I had all these CRC models with their parameters in Rust code, the
compiler would be able to optimize them much better than if I had read the
parameters from the JSON file at runtime. As an example of this, consider a
32-bit CRC algorithm that at the end performs `res ^= 0xFFFFFFFF`. The compiler
was smart enough to turn this into a `not` instruction as you can see in this
disassembly.

```objdump
00000000000042d0 crcmod::Lookup::digest::h9e4b12aea2f2b04c:
  ...
  4357:       f7 d0   notl    %eax
  4359:       c3      retq
```

With this approach I was able to achieve a high level of performance without the
need for any hand-written assembly.

It is nice to have fast code, but it is more important that the code is correct.
This is why we need tests. In the JSON database of CRC algorithms I have a
"check", "residue" and "codeword" values. The check value is the output of the
CRC given an input string "123456789" and codewords are valid CRC sequences,
meaning that their checksum equals the "residue" value. I created another
template for generating the tests that looked like this:

```rust
use crate::{Model, Bitwise};
use crate::models::*;

const CHECK_INPUT: &[u8] = b"123456789";

{% for m in models %}
  #[test]
  fn {{ m.name_lower }}_bitwise_check() {
      // ...
      assert_eq!(
        {{ m.name_camel }}::digest(CHECK_INPUT),
        {{ m.name_camel }}::CHECK
      );
  }

  {% for codeword in m.codewords %}
    #[test]
    fn {{ m.name_lower }}_bitwise_codeword_{{ loop.index }}() {
        // ...
        assert!({{ m.name_camel }}::validate(&codeword));
    }
  {% endfor %}
{% endfor %}
```

Running the tests produces the following results.

```bash
$ cargo test
...
test tests::crc8wcdma_bitwise_codeword_7 ... ok
test tests::crc8wcdma_bitwise_codeword_8 ... ok
test tests::crc8wcdma_bitwise_codeword_6 ... ok
test tests::crc8wcdma_bitwise_codeword_9 ... ok

test result: ok. 326 passed; 0 failed; 0 ignored
```

Of course I didn't get all 326 tests to pass from the start. This was a classic
case of test driven development as we have discussed at Test classes from PBA.

This amount of tests should provide a reasonable guarantee of correctness due to
the large sensitivity of CRCs to input changes. Of course, some CRC models have
more test data than others, based on how many codewords were provided in the
original source.

## Functional Emulation

After I had the data transport layer designed, I wanted to test it. One way to
test it was to use actual hardware but I wanted the ability to see the results
without needing the hardware. That is why I created a functional emulator of the
hardware. It is a program that receives the serial data on stdin and displays
the result in a window. With the help of `socat` utility in Linux, it is easy to
create a virtual serial port that pipes the data to stdin of the emulator. Here
are some screenshots of the emulator running.

![Emulator screenshot using `volume_circle.py`
script](./assets/emu_circle.png){width=40%}

![Emulator screenshot using `fft.py` script](./assets/emu_fft.png){width=40%}

## Fuzzing

To test the robustness of this system I used fuzz testing along with regular
unit tests. Fuzz testing tries to generate inputs to your program and cause the
program to crash. Some fuzzers instrument the code at compile time, giving them
the knowledge about branches and conditions that can be used to generate inputs
to test all branches of your code. Other fuzzers try to monitor the runtime
parameters of your program and use evolutionary algorithms to create inputs that
are best at crashing your program. As long as you can compile code with these
fuzzer instrumentations, I would recommend you use them because they have more
information about the code and will hit all code paths quicker than the runtime
ones.

American Fuzzy Lop [@michal_zalewski_american-fuzzy-lop_2019] (not to be
confused with the rabbit breed), or AFL for short, is a very mature fuzzer that
instruments your code. It gives you executables that wrap gcc and clang that you
use to compile and run your program.

When you launch the program, AFL start doing its job and displays its dashboard
with various statistics as you can see in figure \ref{afl}.

![American Fuzzy Lop testing COBS
decoding.\label{afl}](./assets/afl.png){width=100%}

Another popular fuzzer comes from the works of LLVM, called libFuzzer
[@llvm_libfuzzer_2019]. LLVM team has created great tooling around the compiler
suite. libFuzzer uses the existing coverage code sanitizer to determine which
code paths were hit and then mutate the input to increase the coverage. I tried
to run this fuzzer but unfortunately, was unable to do so, because my code
relies on dynamic libraries like libc and libpython and the Rust tooling was not
happy about that.

Because AFL is easier to setup for C/C++ programs, I tested some C code
responsible for COBS decoding that runs on the hardware that our software talks
to. After letting AFL run for some time it didn't manage to crash it even once.
I wrote that piece of code very carefully and with robustness in mind so seeing
this result was very rewarding. Of course, have I had more time, fuzzing more
pieces of code would be advised.

## Achieving soft real-time

After having implemented mostly all of the functionality, I did a test run that
collected timing information about the processing stages and here you can see
the data visualized.

![Timing diagram of sampling, preprocessing, scripting and output stages
respectively at 9600 baud. Numbers represent buffer ids being
processed.\label{9600}](./assets/9600.png){width=100%}

Originally the speed of the serial link used to speak to the device was 9600
baud. It is a very standard low-speed baud rate that I thought would be
sufficient to deliver the frames in time. However, from the timing diagram in
figure \ref{9600}, it was clear that sending the data to the device was taking
longer than I expected. So long, that the output frames started piling up in
front of the output thread. In the diagram, you can see that while we are
processing buffer 21 we are still outputting an old buffer 13. This meant that
the device was getting frames not only slower that it should, but the frames
were also representing old data, audio data that went by seconds ago. This was
completely unacceptable for audio visualizations.

So let's do some basic calculations to see how long it takes to send a frame of
data to the device. We are using 9600 baud 8-N-1 serial configuration, which
means that for each byte we are transferring 1 start bit + 8 data bits + 1 stop
bit, so in total 10 symbols for each byte.

$$ t_{symbol} = 1/9600 s $$
$$ t_{byte} = 10 \times t_{symbol} = 1/960 s $$

And we have 28 bytes per frame.

$$ t_{frame} = 28 \times t_{byte} \approx 29 ms $$

As we can see, the frame time is more than our 23.5 ms limit. So this behaviour
of the system could have been foreseen, had I done some math beforehand.

Fortunately, there is a simple solution to this, increasing the baud rate of the
serial link. This is the time diagram after increasing the baud rate to 115200.

![Timing diagram of sampling, preprocessing, scripting and output stages
respectively at 115200 baud. Numbers represent buffer ids being
processed.\label{115200}](./assets/115200.png){width=100%}

As you can see, now we have achieved timely output frame delivery. However, to
be able to say that this system is real-time, we will need more data. Design of
each real-time application will need to consider the risk of system not
responding in time. For our system having one frame delivered late would not be
a problem, since most people wouldn't even notice. But for safety critical
systems like the on-board computers in the cockpit of an airplane, this late
response might cause serious harm. This is why we need to determine how often
our system responds too late and put an upper bound on it. For firm/soft
real-time systems we usually turn to statistical models, while for hard
real-time we might need to prove the maximum latency of system using various
mathematical and computational models.

I collected the timing information from a longer run of the program and analysed
it.

![Box plot showing execution time of different processing
stages.\label{stats}](./assets/box_plot.png)

In figure \ref{stats}, we can see that even the slowest executions of the
preprocessing, scripting or output stages are still faster than the average
buffer delivery time (sampling stage). So based on this test I can conclude that
we have achieved soft real-time.

More serious systems are subjected to much more rigorous testing. The system
might for example respond slower on a busy system and that's why we need to test
various factors, like system load, and how they influence the performance of the
system.

\pagebreak

# Result

Primary objective of creating a real-time application framework for audio
visualization has been achieved. I think that most the requirements outlined in
the beginning of this document were satisfyingly addressed and implemented
accordingly. I also made some pictures and a video showing the final result
[@petermajchrakVizzerResults2019].

![Photo of the device working using `fft.py` script.](./assets/result_fft.jpg)

The secondary goal of the project, namely exploring the differences between
real-time and regular software, has also been fruitful. I have explored many
areas of software development and architecture. I found that in most cases the
processes and knowledge used in regular software development can still be used
and applied to real-time systems, with some exceptions that have to deal with
performance. Methodologies used in real-time systems' development should not be
some restrictive subset of known methodologies, instead we should try to use the
best tool for the job and if that best tool is agile we should not be scared of
it.

## Future improvements

Apart from the actual development of some scripts that can demonstrate all the
functionality of the framework, there is not much more to be done. Due to the
modular architecture used, it would be nice to write more input and output
plugins of course. All development has been done on a Linux based OS and should
work on all unix-like systems with no modification needed. Support for other
operating systems should be included in the future. It is mostly a matter of
having the right input and output plugins because all the Rust code for this
application is portable, the only possible portability problems could be caused
by dependencies.

Alternative scripting languages could also be used in the future. By
modularizing the scripting implementation we would allow the user to use
whatever scripting language he prefers, whether that is Lua, Python, JS or even
Lisp for example.

For the firmware I also left some thing out due to time limitations. Things like
CRC check and more testing would be advisable.

# Conclusion & Reflections

During this project I realized that no matter how different real-time
applications are compared to regular software, we should not limit ourselves to
the same old way of doing software. We should embrace new technologies that can
help us achieve our goals and requirements. In this project, I used some novel
approaches that people rarely use in real-time, robust or safety-critical
software, stuff like code generation, emulation, fuzzing and most importantly a
programming language that's not just a portable assembler, like C, but a valuable
tool for expressing high level concepts and turning them down to efficient
executables.

Forget the old days of Fortran and C for mission-critical systems, instead
embrace the advanced type system and safety guarantees of Rust or other modern
tooling. And I am really tired of people saying "this is how we've always done
things", well, maybe it's time for change, we no longer need to be stuck in the
stone age of safety-critical systems development.

We have learned a lot about software development in PBA lectures, and there were
a lot of great ideas presented. In this project I tried to expand on some of the
topics. For example taking the idea of unit tests from the testing lectures and
extending it to the generation of unit tests based on a specification file or
fuzz testing. Similarly system integration classes where we discussed messaging
patterns that I was able to use in a real-time and low-latency system, something
that the original lectures did not have in mind. Other topics like development
methodologies and architecture design are equally important because without them
we wouldn't have the framework which we use to do the actual implementation.

I think the project was good because I explored a new area and had fun at the
same time. Having the inner motivation to learn new things is something people
often lack and it results in developers who write bad code because they don't
see things from other perspectives. All they see is the way they originally
learned it and nothing has changed in their mind for years. This is very
different from reality, however, where we are moving at an ever increasing pace,
especially in the field of technology.

\pagebreak

# Bibliography

::: {#refs}
:::

\pagebreak

# Appendix

## CRC Code & fft.py python visualization script

```rust
pub trait CrcNum = PrimInt + LookupReverse + WrappingShl;

pub trait Model<T: CrcNum>: Sized {
    const POLY: T;
    const INIT: T;
    const REFIN: bool;
    const REFOUT: bool;
    const XOROUT: T;
    const CHECK: T;
    const RESIDUE: T;
}

pub trait Bitwise<T: CrcNum>: Model<T> {
    fn digest(data: &[u8]) -> T {
        let mut crc = Crc::<T, Self, BitwiseStrategy>::new();
        crc.update(data);
        crc.digest()
    }

    fn check(data: &[u8]) -> bool {
        Self::digest(data) ^ Self::XOROUT == Self::RESIDUE
    }
}

pub trait Lookup<T: CrcNum>: Model<T> {
    const TABLE: [T; 256];

    fn digest(data: &[u8]) -> T {
        let mut crc = Crc::<T, Self, LookupStrategy<T, Self>>::new();
        crc.update(data);
        crc.digest()
    }

    fn check(data: &[u8]) -> bool {
        Self::digest(data) ^ Self::XOROUT == Self::RESIDUE
    }
}

pub trait Strategy<T: CrcNum> {
    fn crc_rem(rem: T, dividend: u8, divisor: T) -> T;
}

pub struct Crc<T: CrcNum, M: Model<T>, S: Strategy<T>> {
    rem: T,
    _pd: PhantomData<(M, S)>,
}

impl<T: CrcNum, M: Model<T>, S: Strategy<T>> Crc<T, M, S> {
    pub fn new() -> Crc<T, M, S> {
        Crc {
            rem: M::INIT,
            _pd: PhantomData,
        }
    }

    pub fn update(&mut self, data: &[u8]) {
        for &byte in data {
            let byte = if M::REFIN { byte.swap_bits() } else { byte };
            self.rem = S::crc_rem(self.rem, byte, M::POLY);
        }
    }

    pub fn digest(&mut self) -> T {
        M::XOROUT
            ^ if M::REFOUT {
                self.rem.swap_bits()
            } else {
                self.rem
            }
    }
}
```

```python
from math import ceil, log10
from numpy import mean, clip

def toByte(n):
    res = 0
    for i in range(n):
        res >>= 1
        res |= 0x80
    return res

def map(x, xmin, xmax, ymin, ymax):
    return ymin + (ymax-xmin)*(x-xmin)/(xmax-xmin)

def downsample(arr, bin_size):
    bin_count = int(ceil(len(arr) / bin_size))
    pars = [(x*bin_size, (x+1)*bin_size) for x in range(bin_count)]
    return [mean(arr[start:end]) for (start, end) in pars]

def visualize(input):
    fft = downsample(input.fft, 8)
    fft = [10*log10(x) for x in fft[0:24]]
    fft = [map(x, -15, 60, 0, 8) for x in fft]
    fft = clip(fft, 0, 8)
    return [toByte(int(round(x))) for x in fft]
```

## Behind the scenes - Making of this document

The content of this document is written in Markdown format using a plain text
editor [@peter_majchrak_bachelors-thesis_2019]. Processing and conversion into
an EPUB and PDF are done using pandoc. I have also setup CI, which allows me to
commit just my markdown changes and then I can download the resulting build
artifacts from GitLab. This allows me to focus on the content instead of having
to constantly fight with formatting and layout. CI is something I use every day
at work, so I like to use it on all my projects.

Pandoc has a lot of advantages. It is easily extensible by the use of filters.
When pandoc parses an input file, it creates an AST representation of the file
and passes it through all the filters you specify. These filters are really easy
to write, they are just regular executables that receive the JSON AST on stdin
and print the modified JSON AST to stdout. For example I wrote a pandoc filter
in Rust that looks for code blocks (python and bash) with a "run" class, runs
the code and puts the output below the code block
[@peter_majchrak_pandoc-run-code_2019]. This allows me to code simple scripts
or expressions directly in my markdown files without having to worry that my
original code and the markdown copy diverge, i.e. you will always get the true
output of the commands in the document.

The timing diagrams in this documents were made using Wavedrom
[@aliaksei_chapyzhenka_wavedrom_2019]. I manually instrumented the application
with timing measurements and saved the result in a file. Then I used a jupyter
notebook to transform this data into JSON that wavedrom expects. All scripts and
notebooks are available in the git repo [@peter_majchrak_bachelors-thesis_2019].
