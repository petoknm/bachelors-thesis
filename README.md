# bachelors-thesis

My bachelors's thesis (supposedly)

## Requirements

For compiling the thesis you're gonna need:
  - [pandoc](https://pandoc.org/)
    - [pantable](https://github.com/ickc/pantable)
  - LaTeX
    - [texlive](https://www.tug.org/texlive/)
  - [make](https://www.gnu.org/software/make/)

## Compiling

`make thesis` or `make presentation` or `make all`

## Development

`.on-save.json` file takes care of compiling the thesis and refreshing `mupdf`
whenever you save any markdown file.

Bibliography was created using [Zotero](https://www.zotero.org/) and exported
using
[zotero-better-bibtex](https://github.com/retorquere/zotero-better-bibtex)
