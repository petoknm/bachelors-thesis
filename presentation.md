% Application of modern techniques to real-time and robust system design
% Peter Majchrak
% June 18, 2019

# Introduction

## What are we dealing with

![](./assets/led_clock.jpg)

---

Objectives:

- Transfer skills from PBA to real-time systems' development
- Apply good development practices/methodologies to the development
- Ensure robustness through system testing

# Architecture

## Low-latency message passing

![](./assets/pipeline.png){width=50%}

---

## Timing diagram

![](./assets/timing.png)

---

## Interfaces (traits) / Plugins

![](./assets/modular_architecture.png)

# Testing

## Specification based testing

![](./assets/reveng.png)

---

```json
{
  "width": 32,                "value_type": "u32",
  "poly": "0x04c11db7",       "init": "0xffffffff",
  "refin": "false",           "refout": "false",
  "xorout": "0xffffffff",     "check": "0xfc891918",
  "residue": "0xc704dd7b",    "codewords": ["redacted"],
  "name": "CRC-32/BZIP2",     "name_camel": "Crc32Bzip2",
  "name_lower": "crc32bzip2", "table": "redacted"
}
```

---

```rust
use crate::*;

{% for m in models %}
  pub struct {{ m.name_camel }};

  impl Model<{{ m.value_type }}> for {{ m.name_camel }} {
      const POLY: {{ m.value_type }} = {{ m.poly }};
      const INIT: {{ m.value_type }} = {{ m.init }};
      const REFIN: bool = {{ m.refin }};
      const REFOUT: bool = {{ m.refout }};
      const XOROUT: {{ m.value_type }} = {{ m.xorout }};
      const CHECK: {{ m.value_type }} = {{ m.check }};
      const RESIDUE: {{ m.value_type }} = {{ m.residue }};
  }
{% endfor %}
```

---

## TDD

```bash
$ cargo test
...
test tests::crc8wcdma_bitwise_codeword_7 ... ok
test tests::crc8wcdma_bitwise_codeword_8 ... ok
test tests::crc8wcdma_bitwise_codeword_6 ... ok
test tests::crc8wcdma_bitwise_codeword_9 ... ok

test result: ok. 326 passed; 0 failed; 0 ignored
```

---

## Fuzzing

![](./assets/afl.png){width=100%}

# Emulation

![](./assets/emu_circle.png){width=45%}
![](./assets/emu_fft.png){width=45%}

# Results

Objectives achieved:

- Applied various testing strategies from PBA (and more)
- Applied integration, architecture and software design from PBA
- Applied good development practices
  - Automated tests, CI, etc

[Photos/Videos](https://photos.app.goo.gl/cJ2vWv2NPoEMSeVWA)
