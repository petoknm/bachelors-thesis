.PHONY: all clean

all:  				thesis presentation
thesis:  			thesis.pdf
presentation: presentation.pdf

thesis.pdf: thesis.md
	pandoc -F pandoc-citeproc -F pantable $< -o $@

presentation.pdf: presentation.md
	pandoc -t beamer $< -o $@

clean:
	rm -f thesis.pdf presentation.pdf
